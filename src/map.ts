import {Cell} from "./cell";
import {Point} from "./point";

export class Map {
	private _width: number;
	private _height: number;
	private _cells: Array<Array<Cell>>;

	constructor(width?: number, height?: number) {
		if (width && height) {
			this.init(width, height);
		}
	}

	get width() {
		return this._width;
	}

	get height() {
		return this._height;
	}

	public init(width: number, height: number) {
		this._width = width;
		this._height = height;
		this._initCells();
	}

	private _initCells(): void {
		this._cells = [];
		for (let y = 0; y < this.height; ++y) {
			this._cells[y] = [];
			for (let x = 0; x < this.width; ++x) {
				this._cells[y][x] = new Cell(Cell.TYPE_UNDEFINED);
			}
		}
	}

	public setCellType(position: Point, type: number): void;
	public setCellType(x: number, y: number, type: number): void;
	public setCellType(arg1: any, arg2: number, arg3?: number) {
		this._checkIfInitialised();
		let x: number = arg1;
		let y: number = arg2;
		let type: number = arg3;

		if (arg1 instanceof Point) {
			type = arg2;
			x = arg1.x;
			y = arg1.y;
		}

		this._cells[y][x].type = type;
	}

	public getCellType(position: Point): number;
	public getCellType(x: number, y: number): number;
	public getCellType(arg1: any, arg2?: number): number {
		this._checkIfInitialised();
		let x: number = arg1;
		let y: number = arg2;

		if (arg1 instanceof Point) {
			x = arg1.x;
			y = arg1.y;
		}
		return x >= 0 && x < this.width && y >= 0 && y < this.height ?
			this._cells[y][x].type :
			Cell.TYPE_UNDEFINED;
	}

	public containsInBounds(position: Point): boolean;
	public containsInBounds(position: Point, offset: Point): boolean;
	public containsInBounds(position: Point, offset?: Point): boolean {
		if (!offset) {
			offset = new Point(0, 0);
		}
		const normalisedOffset = new Point(
			Math.max(Math.min(offset.x, 1), -1),
			Math.max(Math.min(offset.y, 1), -1)
		);
		const newPosition = position.translate(normalisedOffset).translate(normalisedOffset);

		return newPosition.x > 0 && newPosition.x < this.width - 1 &&
			newPosition.y > 0 && newPosition.y < this.height - 1;
	}

	public forEachCell(callback: (x: number, y: number, cell: Cell) => void) {
		this._checkIfInitialised();
		this._cells.forEach((row, y) => {
			row.forEach((cell, x) => {
				callback(x, y, cell);
			});
		});
	}

	private _checkIfInitialised() {
		if (!this.width || !this.height) {
			throw new Error("Please set the map dimensions first using the Map#init() call.");
		}
	}
}
