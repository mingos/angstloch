import {Point} from "./point";

export const Offsets = [
	new Point(0, -1),
	new Point(1, 0),
	new Point(0, 1),
	new Point(-1, 0)
];
