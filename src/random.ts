export class Random {
	public static int(min: number, max: number): number {
		if (max === min) {
			return min;
		} else if (min > max) {
			let tmp = min;
			min = max;
			max = tmp;
		}
		return min + Math.floor(Math.random() * ((max - min) + 1));
	}

	public static odd(min: number, max: number): number {
		if (max === min) {
			return (min % 2 === 0) ? null : min;
		} else if (min > max) {
			let tmp = min;
			min = max;
			max = tmp;
		}

		if (min % 2 === 0) {
			min++;
		}
		if (max % 2 === 0) {
			max--;
		}

		return min + 2 * Math.floor(Math.random() * ((max - min) / 2 + 1));
	}

	public static float(): number {
		return Math.random();
	}

	public static listElement<T>(list: Array<T>): T {
		return list[Random.int(0, list.length - 1)];
	}
}
