export class Cell {
	public static TYPE_UNDEFINED: number = 0;
	public static TYPE_FLOOR: number = 1;
	public static TYPE_WALL: number = 2;
	public static TYPE_DOOR: number = 3;

	public type: number;

	constructor(type: number) {
		this.type = type;
	}
}
