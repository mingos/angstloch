export class Point {
	private _x: number;
	private _y: number;

	constructor(x: number, y: number) {
		this._x = x;
		this._y = y;
	}

	get x() {
		return this._x;
	}

	get y() {
		return this._y;
	}

	public translate(dx: number, dy: number): Point;
	public translate(point: Point): Point;
	public translate(arg1: any, arg2?: number): Point {
		if (arg1 instanceof Point) {
			arg2 = arg1.y;
			arg1 = arg1.x;
		}
		return new Point(this.x + arg1, this.y + arg2);
	}
}
