import {Point} from "./point";
export const Utils = {
	getOppositeOffset(offset: Point): Point {
		return new Point(-offset.x, -offset.y);
	},
	getPerpendicularOffset(offset: Point, point2?: Point): Point {
		if (point2) {
			offset = point2.translate(-offset.x, -offset.y);
		}
		return new Point(offset.y, -offset.x);
	}
};
