"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var cell_1 = require("./cell");
var point_1 = require("./point");
var Map = (function () {
    function Map(width, height) {
        if (width && height) {
            this.init(width, height);
        }
    }
    Object.defineProperty(Map.prototype, "width", {
        get: function () {
            return this._width;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Map.prototype, "height", {
        get: function () {
            return this._height;
        },
        enumerable: true,
        configurable: true
    });
    Map.prototype.init = function (width, height) {
        this._width = width;
        this._height = height;
        this._initCells();
    };
    Map.prototype._initCells = function () {
        this._cells = [];
        for (var y = 0; y < this.height; ++y) {
            this._cells[y] = [];
            for (var x = 0; x < this.width; ++x) {
                this._cells[y][x] = new cell_1.Cell(cell_1.Cell.TYPE_UNDEFINED);
            }
        }
    };
    Map.prototype.setCellType = function (arg1, arg2, arg3) {
        this._checkIfInitialised();
        var x = arg1;
        var y = arg2;
        var type = arg3;
        if (arg1 instanceof point_1.Point) {
            type = arg2;
            x = arg1.x;
            y = arg1.y;
        }
        this._cells[y][x].type = type;
    };
    Map.prototype.getCellType = function (arg1, arg2) {
        this._checkIfInitialised();
        var x = arg1;
        var y = arg2;
        if (arg1 instanceof point_1.Point) {
            x = arg1.x;
            y = arg1.y;
        }
        return x >= 0 && x < this.width && y >= 0 && y < this.height ?
            this._cells[y][x].type :
            cell_1.Cell.TYPE_UNDEFINED;
    };
    Map.prototype.containsInBounds = function (position, offset) {
        if (!offset) {
            offset = new point_1.Point(0, 0);
        }
        var normalisedOffset = new point_1.Point(Math.max(Math.min(offset.x, 1), -1), Math.max(Math.min(offset.y, 1), -1));
        var newPosition = position.translate(normalisedOffset).translate(normalisedOffset);
        return newPosition.x > 0 && newPosition.x < this.width - 1 &&
            newPosition.y > 0 && newPosition.y < this.height - 1;
    };
    Map.prototype.forEachCell = function (callback) {
        this._checkIfInitialised();
        this._cells.forEach(function (row, y) {
            row.forEach(function (cell, x) {
                callback(x, y, cell);
            });
        });
    };
    Map.prototype._checkIfInitialised = function () {
        if (!this.width || !this.height) {
            throw new Error("Please set the map dimensions first using the Map#init() call.");
        }
    };
    return Map;
}());
exports.Map = Map;
