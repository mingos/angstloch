"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Cell = (function () {
    function Cell(type) {
        this.type = type;
    }
    return Cell;
}());
Cell.TYPE_UNDEFINED = 0;
Cell.TYPE_FLOOR = 1;
Cell.TYPE_WALL = 2;
Cell.TYPE_DOOR = 3;
exports.Cell = Cell;
