"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Point = (function () {
    function Point(x, y) {
        this._x = x;
        this._y = y;
    }
    Object.defineProperty(Point.prototype, "x", {
        get: function () {
            return this._x;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Point.prototype, "y", {
        get: function () {
            return this._y;
        },
        enumerable: true,
        configurable: true
    });
    Point.prototype.translate = function (arg1, arg2) {
        if (arg1 instanceof Point) {
            arg2 = arg1.y;
            arg1 = arg1.x;
        }
        return new Point(this.x + arg1, this.y + arg2);
    };
    return Point;
}());
exports.Point = Point;
