"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Random = (function () {
    function Random() {
    }
    Random.int = function (min, max) {
        if (max === min) {
            return min;
        }
        else if (min > max) {
            var tmp = min;
            min = max;
            max = tmp;
        }
        return min + Math.floor(Math.random() * ((max - min) + 1));
    };
    Random.odd = function (min, max) {
        if (max === min) {
            return (min % 2 === 0) ? null : min;
        }
        else if (min > max) {
            var tmp = min;
            min = max;
            max = tmp;
        }
        if (min % 2 === 0) {
            min++;
        }
        if (max % 2 === 0) {
            max--;
        }
        return min + 2 * Math.floor(Math.random() * ((max - min) / 2 + 1));
    };
    Random.float = function () {
        return Math.random();
    };
    Random.listElement = function (list) {
        return list[Random.int(0, list.length - 1)];
    };
    return Random;
}());
exports.Random = Random;
