"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("./point");
exports.Utils = {
    getOppositeOffset: function (offset) {
        return new point_1.Point(-offset.x, -offset.y);
    },
    getPerpendicularOffset: function (offset, point2) {
        if (point2) {
            offset = point2.translate(-offset.x, -offset.y);
        }
        return new point_1.Point(offset.y, -offset.x);
    }
};
