"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var point_1 = require("./point");
exports.Offsets = [
    new point_1.Point(0, -1),
    new point_1.Point(1, 0),
    new point_1.Point(0, 1),
    new point_1.Point(-1, 0)
];
