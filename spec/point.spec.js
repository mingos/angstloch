const Point = require("../lib/point").Point;
const expect = require("chai").expect;

describe("Point", () => {
	it("should translate a point by coordinates", () => {
		// given
		let point = new Point(5, 5);

		// when
		let result = point.translate(1, -1);

		// then
		expect(result).to.deep.equal(new Point(6, 4));
	});

	it("should translate a point by another point", () => {
		// given
		let point = new Point(5, 5);
		let translation = new Point(1, -1);

		// when
		let result = point.translate(translation);

		// then
		expect(result).to.deep.equal(new Point(6, 4));
	});

	it("should return new Point instance on translation", () => {
		// given
		let point = new Point(5, 5);
		let translation = new Point(1, -1);

		// when
		let result = point.translate(translation);

		// then
		expect(result).not.to.equal(point);
	});
});
