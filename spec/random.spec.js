const Random = require("../lib/random").Random;
const expect = require("chai").expect;

describe("Random", () => {
	it("should generate random integers within specified range", () => {
		// given
		let results = [];

		// when
		for (let i = 0; i < 100; ++i) {
			results.push(Random.int(-10, 10));
		}

		// then
		results.forEach(result => expect(result).to.be.gte(-10));
		results.forEach(result => expect(result).to.be.lte(10));
	});

	it("should generate random odd integers", () => {
		// given
		let results = [];

		// when
		for (let i = 0; i < 100; ++i) {
			results.push(Random.odd(i - 100, i));
		}

		// then
		results.forEach(result => expect(Math.abs(result % 2)).to.equal(1));
	});

	it("should generate random odd integers within specified range", () => {
		// given
		let results = [];

		// when
		for (let i = 0; i < 100; ++i) {
			results.push(Random.odd(-10, 10));
		}

		// then
		results.forEach(result => expect(result).to.be.gte(-10));
		results.forEach(result => expect(result).to.be.lte(10));
	});

	it("should return null when generating odd numbers from non-odd range", () => {
		// when
		let result = Random.odd(4, 4);

		// then
		expect(result).to.be.null;
	});

	it("should return an element of the input list", () => {
		// given
		let list = [{}, {}, {}];
		let results = [];

		// when
		for (let i = 0; i < 100; ++i) {
			results.push(Random.listElement(list));
		}

		// then
		results.forEach(result => expect(list.indexOf(result)).to.be.gte(0));
		results.forEach(result => expect(list.indexOf(result)).to.be.lt(3));
	});

	it("should return different elements of the input list", () => {
		// given
		let list = [{}, {}, {}];
		let results = [];

		// when
		for (let i = 0; i < 100; ++i) {
			results.push(Random.listElement(list));
		}

		// then
		let indices = results
			.map(result => list.indexOf(result))
			.filter((result, index, self) => self.indexOf(result) === index);
		expect(indices).to.have.lengthOf(3);
	});
});
