const Utils = require("../lib/utils").Utils;
const Point = require("../lib/point").Point;
const expect = require("chai").expect;

describe("Utils", () => {
	it("should get opposite offset", () => {
		// given
		let offset = new Point(3, -7);
		let expected = new Point(-3, 7);

		// when
		let result = Utils.getOppositeOffset(offset);

		// then
		expect(result).to.deep.equal(expected);
	});

	it("should get perpendicular offset", () => {
		// given
		let offset = new Point(3, -7);
		let expected = new Point(-7, -3);

		// when
		let result = Utils.getPerpendicularOffset(offset);

		// then
		expect(result).to.deep.equal(expected);
	});

	it("should get perpendicular offset relative to a line", () => {
		// given
		let point1 = new Point(10, 10);
		let point2 = new Point(13, 9);
		let expected = new Point(-1, -3);

		// when
		let result = Utils.getPerpendicularOffset(point1, point2);

		// then
		expect(result).to.deep.equal(expected);
	});
});
