const Map = require("../lib/map").Map;
const Cell = require("../lib/cell").Cell;
const Point = require("../lib/point").Point;
const expect = require("chai").expect;
const sinon = require("sinon");
const assert = sinon.assert;
const spy = sinon.spy;

describe("Map", () => {
	let map;

	beforeEach(() => {
		map = new Map();
	});

	it("should init its internal variables", () => {
		// when
		map.init(5, 5);

		// then
		expect(map.width).to.equal(5);
		expect(map.height).to.equal(5);
	});

	it("should throw when operating on cells without initialisation", () => {
		expect(() => map.getCellType(0, 0)).to.throw();
		expect(() => map.setCellType(0, 0, Cell.TYPE_FLOOR)).to.throw();
		expect(() => map.forEachCell(() => {})).to.throw();
	});

	it("should get and set cell types", () => {
		// given
		map.init(5, 5);

		// when
		map.setCellType(0, 0, Cell.TYPE_FLOOR);
		map.setCellType(new Point(1, 0), Cell.TYPE_WALL);

		// then
		expect(map.getCellType(0, 0)).to.equal(Cell.TYPE_FLOOR);
		expect(map.getCellType(new Point(1, 0))).to.equal(Cell.TYPE_WALL);
		expect(map.getCellType(0, 1)).to.equal(Cell.TYPE_UNDEFINED);
	});

	it("should iterate over cells", () => {
		// given
		map.init(2, 2);
		map.setCellType(0, 0, Cell.TYPE_FLOOR);
		map.setCellType(1, 0, Cell.TYPE_FLOOR);
		map.setCellType(0, 1, Cell.TYPE_WALL);
		map.setCellType(1, 1, Cell.TYPE_WALL);

		let spyFn = spy();

		// when
		map.forEachCell(spyFn);

		// then
		assert.callCount(spyFn, 4);
		assert.calledWith(spyFn, 0, 0, new Cell(Cell.TYPE_FLOOR));
		assert.calledWith(spyFn, 1, 0, new Cell(Cell.TYPE_FLOOR));
		assert.calledWith(spyFn, 0, 1, new Cell(Cell.TYPE_WALL));
		assert.calledWith(spyFn, 1, 1, new Cell(Cell.TYPE_WALL));
	});

	it("should ensure a cell is within the map bounds", () => {
		// given
		map.init(3, 3);

		// when
		let result = map.containsInBounds(new Point(1, 1));

		// then
		expect(result).to.be.true;
	});

	it("should ensure a cell with offset is within the map bounds", () => {
		// given
		map.init(7, 7);

		// when
		let result = map.containsInBounds(new Point(3, 3), new Point(-1, 0)) &&
			map.containsInBounds(new Point(3, 3), new Point(0, -1)) &&
			map.containsInBounds(new Point(3, 3), new Point(1, 0)) &&
			map.containsInBounds(new Point(3, 3), new Point(0, 1));

		// then
		expect(result).to.be.true;
	});

	it("should ensure a cell with large offset is within the map bounds", () => {
		// given
		map.init(7, 7);

		// when
		let result = map.containsInBounds(new Point(3, 3), new Point(-5, 0)) &&
			map.containsInBounds(new Point(3, 3), new Point(0, -5)) &&
			map.containsInBounds(new Point(3, 3), new Point(5, 0)) &&
			map.containsInBounds(new Point(3, 3), new Point(0, 5));


		// then
		expect(result).to.be.true;
	});

	it("should ensure a cell is not within the map bounds", () => {
		// given
		map.init(3, 3);

		// when
		let result = map.containsInBounds(new Point(1, 1), new Point(-1, 0)) ||
			map.containsInBounds(new Point(1, 1), new Point(0, -1)) ||
			map.containsInBounds(new Point(1, 1), new Point(1, 0)) ||
			map.containsInBounds(new Point(1, 1), new Point(0, 1)) ||
			map.containsInBounds(new Point(3, 3));

		// then
		expect(result).to.be.false;
	});
});
