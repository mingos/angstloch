export declare class Random {
    static int(min: number, max: number): number;
    static odd(min: number, max: number): number;
    static float(): number;
    static listElement<T>(list: Array<T>): T;
}
