export declare class Cell {
    static TYPE_UNDEFINED: number;
    static TYPE_FLOOR: number;
    static TYPE_WALL: number;
    static TYPE_DOOR: number;
    type: number;
    constructor(type: number);
}
