import { Point } from "./point";
export declare const Utils: {
    getOppositeOffset(offset: Point): Point;
    getPerpendicularOffset(offset: Point, point2?: Point): Point;
};
