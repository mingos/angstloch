import { Cell } from "./cell";
import { Point } from "./point";
export declare class Map {
    private _width;
    private _height;
    private _cells;
    constructor(width?: number, height?: number);
    readonly width: number;
    readonly height: number;
    init(width: number, height: number): void;
    private _initCells();
    setCellType(position: Point, type: number): void;
    setCellType(x: number, y: number, type: number): void;
    getCellType(position: Point): number;
    getCellType(x: number, y: number): number;
    containsInBounds(position: Point): boolean;
    containsInBounds(position: Point, offset: Point): boolean;
    forEachCell(callback: (x: number, y: number, cell: Cell) => void): void;
    private _checkIfInitialised();
}
