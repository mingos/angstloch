export declare class Point {
    private _x;
    private _y;
    constructor(x: number, y: number);
    readonly x: number;
    readonly y: number;
    translate(dx: number, dy: number): Point;
    translate(point: Point): Point;
}
